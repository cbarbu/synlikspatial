# README #

This repository allows to fit multi-scale dispersal to spatial snapshots of infestation using spatial statistics and the synthetic likelihood framework.

### What is this repository for? ###

* Make available the code for an article being published
* Allow others to build on these methods to do their own analysis

### How do I get set up? ###

* Install R
* Clone this repository
* install the [yamh package](https://bitbucket.org/cmbce/r-package-yamh/admin/access)
* source from R demonstrateMCMCJerusalen.R and install all needed packages
* you can modify the options in demonstrateMCMCJerusalen.R to modify the fit and run it on your own data

### Contribution guidelines ###

Contributions are welcome, particularly new statistics to be used with yamh, just do a pull request!

